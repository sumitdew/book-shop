package bookshop;

public class Book {
    String bookName;

    public Book(String name) {
        this.bookName = name;
    }

    public String getBookName() {
        return bookName;
    }
}
